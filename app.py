# ----------------------------------------------------------------------------------------------------------------------
# Hex Shoot Server
# ----------------------------------------------------------------------------------------------------------------------

from flask import Flask, render_template

# Managers
from server.config import confMan
from server.managers.socket import socketMan
from server.managers import player

# Socket Handlers
from server.sockets import root

# Routes
from server.routes import test

# ----------------------------------------------------------------------------------------------------------------------

def main():
    app = Flask('hexshoot', static_url_path = '', static_folder = "./dist")

    # Register Routes
    # app.register_blueprint(test.bp)

    # Single Page Configuration
    @app.errorhandler(404)
    def page_not_found(e):
        """
        Because we're running as a single page application, we register for 404 pages in flask, and always return the
        index page. Realistically, we should only do this for routes and not files, but detecting that involved some
        work that I don't feel like doing right now.
        """
        return app.send_static_file('index.html')

    # Run the application
    socketMan.create_server(app, **confMan.config['http'].get('server', {}))

# ----------------------------------------------------------------------------------------------------------------------

# Execute the server
main()

# ----------------------------------------------------------------------------------------------------------------------
