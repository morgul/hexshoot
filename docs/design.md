# Hex Shoot Design Doc

This is meant to be a proof of concept for a game server where users join different 'scenarios' (including custom ones).
Each scenario has a Map (which is a hex grid of specific dimensions, scenery, and npcs). Each player controls a fleet,
and all fleets have zero or more ships. Ships, in turn, have stats and powers. Some powers are built in (movement), while
others are from equipment (weapons, etc).

_Note: For the PoC, I am ignoring equipment, and all powers will be built in. I am also ignoring NPCs_

Game play will proceed in phases, every player getting a turn in each phase. The phases will always be set to:
  * Begin Phase - Some powers may have passive effects in this phase
  * Tactical - Activating any powers available during the tactical phase, as well as deciding movement
  * Combat - Activating any powers available during the combat phase, like weapons
  * End Phase - Some powers may have passive effects in this phase

(Each player must mark 'ready' to transition from Tactical to Combat.)

## Major Classes

## Player
  * Props:
    * `name` - player name
    * `socket` - the socket.io `socket`

This represents a player. In theory, it would know all the juicy details, like what ships and equiment they have unlocked. In the PoC, it's basically just an abstraction, and holds name.

### Scenario
  * Events
      * `phase_changed`
        * args: `phase_name`
      * `scenario_complete`
        * args: `winner`
      * `error`
        * args: `error_obj`
  * States:
      * `waiting_for_players`
        * `on_exit` - emit `phase_changed`.
        * transitions: 
            * `start_scenario` => `begin_phase`
            * condition: if we're at minimum players, and all players have hit 'ready'.
      * `begin_phase`
        * `on_exit` - emit `phase_changed`.
        * transitions:
            * `ship_begin_complete` => `tactical_phase`
            * condition: if all ships have completed
      * `tactical_phase`
        * `on_exit` - emit `phase_changed`.
        * transitions:
            * `tactical_complete` => `combat_phase`
                * condition: if all players have hit 'ready'
      * `combat_phase`
        * `on_exit` - emit `phase_changed`.
        * transitions:
            * `combat_complete` => `end_phase`
                * condition: if all players have hit 'ready'
      * `end_phase`
        * `on_enter` - check for end conditions.
        * `on_exit` - emit `phase_changed`.
        * transitions:
            * `scenario_complete` => `complete`
            * condition: if any of the end conditions have been met
            * `ship_end_complete` => `begin_phase`
            * condition: if all ships have completed
      * `complete`
        * `on_enter` - emit `scenario_complete`
  * Props:
    * `players` - a list of all players in this scenario
    * `ships` - a list of all ships (npc or player controlled)
    * `map` - the map instance

The scenario class keeps track of what state it's in, and informs anything that cares about changes via events.

Scenarios always start in the `waiting_for_players` phase, and only move out of it once either enough players have joined, or have clicked `ready`. From there, we stay in a tight turn look until a victory condition is hit, then it transitions to `complete`, emitting a `scenario_complete` event with `victory` either true or false.

## Map
tbd.

## Ship

  * Events:
    * `destroyed` - indicates the ship has been destroyed
  * Props:
    * `id` - uuid
    * `owner` - the player who owns the ship (None if npc)
    * `name` - the individual name of the ship
    * `class_name` - the class of ship
    * `max_movement` - the maximum movement points (refreshed to this every turn)
    * `max_hull` - the maximum hull points
    * `max_shields` - the maximum shield points
    * `movement` - the current amount of movement points
    * `hull` - the current amount of hull
    * `shields` - the current amount of shields
    * `destroyed` - indicates the ship has been destroyed
