# ----------------------------------------------------------------------------------------------------------------------
# Config Manager
# ----------------------------------------------------------------------------------------------------------------------

import os
from pathlib import Path

import yaml

# ----------------------------------------------------------------------------------------------------------------------

class ConfigManager:
    """
    A class to handle importing config, and modifying it based on what environment we're running in.
    """

    def __init__(self):
        data = {}

        # Load the `env` variable from the system environment variables
        self.__env = os.environ.get('ENVIRONMENT', 'local').lower()

        # Load yaml config file
        with open(self._get_config_path(), 'r') as stream:
            try:
                data = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

        # Pull out the defaults and environment specific overrides
        defaults = {k: v for k, v in data.items() if k != 'environments'}
        overrides = data['environments'][self.__env] or {}

        # Set our internal config
        self.__config = self._merge(overrides, self._merge(defaults, {}))

    # ------------------------------------------------------------------------------------------------------------------
    # Properties
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def config(self):
        return self.__config

    @property
    def env(self):
        return self.__env

    # ------------------------------------------------------------------------------------------------------------------
    # Methods
    # ------------------------------------------------------------------------------------------------------------------

    def _merge(self, source, destination):
        """
        run me with nosetests --with-doctest file.py

        >>> a = { 'first' : { 'all_rows' : { 'pass' : 'dog', 'number' : '1' } } }
        >>> b = { 'first' : { 'all_rows' : { 'fail' : 'cat', 'number' : '5' } } }
        >>> merge(b, a) == { 'first' : { 'all_rows' : { 'pass' : 'dog', 'fail' : 'cat', 'number' : '5' } } }
        True
        """
        for key, value in source.items():
            if isinstance(value, dict):
                # get node or create one
                node = destination.setdefault(key, {})
                self._merge(value, node)
            else:
                destination[key] = value

        return destination

    def _get_config_path(self) -> Path:
        """Returns project root folder."""
        return Path(__file__).parent.parent.joinpath('config.yml')

# ----------------------------------------------------------------------------------------------------------------------

confMan = ConfigManager()
__all__ = confMan

# ----------------------------------------------------------------------------------------------------------------------
