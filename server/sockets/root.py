# ----------------------------------------------------------------------------------------------------------------------
# Root Socket.IO Handlers
# ----------------------------------------------------------------------------------------------------------------------

from flask import request
from flask_socketio import send

# Managers
from server.managers.player import get_player
from server.managers.socket import socketMan
from server.managers.scenario import scenarioMan

# ----------------------------------------------------------------------------------------------------------------------

socketio = socketMan.socketio

# ----------------------------------------------------------------------------------------------------------------------

@socketio.on('set_name')
def set_player_name(payload):
    """ Set the player's name """
    name = payload.get('name')
    player = get_player(request.sid)
    player.set_name(name)

    return socketMan.build_envelope(player)

@socketio.on('join_scenario')
def join_scenario(payload):
    """ Join an existing scenario, by id """
    scenario_id = payload.get('scenario')

    # Find the scenario
    scenario = scenarioMan.get_scenario(scenario_id)

    # Get the player, and force them to join the scenario
    player = get_player(request.sid)
    scenario.join_scenario(player)

    return socketMan.build_envelope({})

@socketio.on('create_scenario')
def create_scenario(payload):
    """ Create a new scenario """
    scenario_type = payload.get('type')

    # Create the scenario
    scenario = scenarioMan.create_scenario(scenario_type)

    # Get the player, and force them to join the scenario
    player = get_player(request.sid)
    scenario.join(player)

    # Return the scenario
    return socketMan.build_envelope(scenario)

# ----------------------------------------------------------------------------------------------------------------------
