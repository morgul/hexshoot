# ----------------------------------------------------------------------------------------------------------------------
# Socket Manager
# ----------------------------------------------------------------------------------------------------------------------

import time
from flask_socketio import SocketIO, emit

# ----------------------------------------------------------------------------------------------------------------------

class SocketManager:
    def __init__(self):
        self.__socketio = SocketIO()

    # ------------------------------------------------------------------------------------------------------------------
    # Properties
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def socketio(self):
        return self.__socketio

    # ------------------------------------------------------------------------------------------------------------------
    # Methods
    # ------------------------------------------------------------------------------------------------------------------

    def create_server(self, app, **kwargs):
        self.__socketio.init_app(app)
        self.__socketio.run(app, **kwargs)

    def build_envelope(self, response):
        """ Builds a response envelope that the server can parse. """
        envelope = { 'error': None, 'payload': None, 'timestamp': time.time() * 1000 }
        if (isinstance(response, Exception)):
            # So, we have to handle exceptions differently
            envelope['error'] = {'name': response.__class__.__name__, 'message': response.message}
        else:
            # Auto convert our custom objects
            to_dict_func = getattr(response, 'to_dict', None)
            if (callable(to_dict_func)):
                response = response.to_dict()

            # Set the payload
            envelope['payload'] = response

        # Send the response
        return envelope

# ----------------------------------------------------------------------------------------------------------------------

socketMan = SocketManager()
__all__ = socketMan

# ----------------------------------------------------------------------------------------------------------------------
