# ----------------------------------------------------------------------------------------------------------------------
# Scenario Manager
# ----------------------------------------------------------------------------------------------------------------------

# Models
from server.game.scenarios.test import TestScenario

# ----------------------------------------------------------------------------------------------------------------------

class ScenarioManager:
    def __init__(self):
        self.__scenarios = {}
        self.__scenarioTypes = {
            'test': TestScenario
        }

    # ------------------------------------------------------------------------------------------------------------------
    # Methods
    # ------------------------------------------------------------------------------------------------------------------

    def list_scenario_types(self):
        return self.__scenarioTypes.keys()

    def create_scenario(self, type):
        Scenario = self.__scenarioTypes[type]
        scenario = Scenario()
        self.__scenarios[scenario.id] = scenario

        return scenario

    def get_scenario(scenario_id):
        return self.__scenarios.get(scenario_id)

# ----------------------------------------------------------------------------------------------------------------------

scenarioMan = ScenarioManager()
__all__ = scenarioMan

# ----------------------------------------------------------------------------------------------------------------------

