# ----------------------------------------------------------------------------------------------------------------------
# Hex Map
# ----------------------------------------------------------------------------------------------------------------------

from abc import ABC, abstractmethod
from math import pow, sqrt

# ----------------------------------------------------------------------------------------------------------------------

class BaseMap(ABC):
    """ A base map representation. """

    def __init__(self, definition={}):
        super().__init__()
        self.__definition = definition

    @abstractmethod
    def calc_distance(from_coord, to_coord):
        """ Calculate using standard distance formula. """
        pass

    @abstractmethod
    def calc_rotation(self, from_rot, to_rot):
        """ Calculate the total number of rotations to arrive at `to_rot` from `from_rot`. """
        pass

# ----------------------------------------------------------------------------------------------------------------------

class HexMap(BaseMap):
    """ A hexoganal grid representation of a map. """
    __rotations = [
        (1, 0, -1),
        (0, 1, -1),
        (-1, 1, 0),
        (-1, 0, 1),
        (0, -1, 1),
        (1, -1, 0),
    ]

    def __init__(self, definition={}):
        super().__init__(definition)

    def calc_distance(self, from_coord, to_coord):
        """
        Calculate assuming the cube_distance formula, as explained [here][].

        [here]: https://www.redblobgames.com/grids/hexagons/#distances
        """
        return max(abs(from_coord[0] - to_coord[0]), abs(from_coord[1] - to_coord[1]), abs(from_coord[2] - to_coord[2]))

    def calc_rotation(self, from_rot, to_rot):
        """
        Calculate the total number of rotations to arrive at `to_rot` from `from_rot`, assuming each rot is a unit
        vector, of the form `(x, y, z)`. This assumes the most optimal direction is used to arrive at the rotation;
        i.e. if the desired rotation is one move away clockwise, and five counter-clockwise, we assume the move to be
        clockwise, and return one.

        >>> m = HexMap()
        >>> m.calc_rotation((1,0,-1), (-1,1,0))
        2
        >>> m.calc_rotation((1,0,-1), (-1,0,1))
        3
        >>> m.calc_rotation((1,0,-1), (0,-1,1))
        2
        >>> m.calc_rotation((1,0,-1), (1,-1,0))
        1
        """

        # Find the indexes of the rotation
        from_index = self.__rotations.index(from_rot)
        to_index = self.__rotations.index(to_rot)

        # This 'cost' is only looking at a clock-wise rotation. Sometimes, it will be more efficient to 'go the other
        # way', but we'll deal with that next.
        cost = abs(from_index - to_index)

        # Going 'the other way' involves taking the total length of all possible rotations, and subtracting the other
        # cost. This makes sense if you actually try it with a hex on paper.
        alt_cost = len(self.__rotations) - cost

        # The correct cost is whichever one of these is the smallest.
        return min(cost, alt_cost)

# ----------------------------------------------------------------------------------------------------------------------
