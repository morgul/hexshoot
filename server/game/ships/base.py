# ----------------------------------------------------------------------------------------------------------------------
# Ship Base Class
# ----------------------------------------------------------------------------------------------------------------------

import uuid

from pyee import BaseEventEmitter

# ----------------------------------------------------------------------------------------------------------------------

class BaseShip(BaseEventEmitter):
    """ A basic representation of a ship. """

    def __init__(self, scenario, player_id, name, definition={}):
        super().__init__()

        self.__position = definition.get('position', (0, 0, 0)
        self.__rotation = definition.get('rotation', (1, 0, -1))
        self.__pending_movement = []

        self.__uuid = uuid.uuid4()
        self.__scenario = scenario
        self.__player_id = player_id
        self.__definition = definition
        self.__name = name
        self.__movement = definition.get('movement', 0)
        self.__hull = definition.get('hull', 1)
        self.__shields = definition.get('shields', 0)

    # ------------------------------------------------------------------------------------------------------------------
    # Properties
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def id(self):
        return self.__uuid

    @property
    def owner(self):
        return self.__player_id

    @property
    def scenario(self):
        return self.__scenario

    @property
    def map(self):
        return self.__scenario.map

    @property
    def name(self):
        return self.__name

    @property
    def class_name(self):
        return self.__definition.name

    @property
    def max_movement(self):
        return self.__definition.movement

    @property
    def max_hull(self):
        return self.__definition.hull

    @property
    def max_shields(self):
        return self.__definition.shields

    @property
    def move_cost(self):
        return self.__definition.move_cost

    @property
    def rot_cost(self):
        return self.__definition.rot_cost

    @property
    def position(self):
        return self.__position

    @property
    def rotation(self):
        return self.__rotation

    @property
    def max_movement(self):
        return self.__definition.movement

    @movement.setter
    def movement(self, val):
        val = min(val, self.max_movement)
        val = max(val, 0)
        self.__movement = val

    @property
    def hull(self):
        return self.__hull

    @hull.setter
    def hull(self, val):
        val = min(val, self.max_hull)
        val = max(val, 0)
        self.__hull = val

    @property
    def shields(self):
        return self.__shields

    @shields.setter
    def shields(self, val):
        val = min(val, self.max_shields)
        val = max(val, 0)
        self.__shields = val

    @property
    def destroyed(self):
        return self.__hull == 0

    # ------------------------------------------------------------------------------------------------------------------
    # Event Handler
    # ------------------------------------------------------------------------------------------------------------------

    @self.scenario.on('phase_changed')
    def on_phase_changed(self, phase):
        lifecycle_hook = self[phase]
        if (lifecycle_hook):
            lifecycle_hook()

    # ------------------------------------------------------------------------------------------------------------------
    # Life Cycle
    # ------------------------------------------------------------------------------------------------------------------

    def begin_phase(self):
        """ Any code that needs to be run when the scenario enters the begin phase """

        # Tell the scenario we're ready
        self.scenario.ship_ready(self.id)

    def tactical_phase(self):
        """ Any code that needs to be run when the scenario enters the tactical phase """
        pass

    def combat_phase(self):
        """ Any code that needs to be run when the scenario enters the combat phase """

        # Apply any pending moves
        self.apply_moves()

    def end_phase(self):
        """ Any code that needs to be run when the scenario enters the end phase. """

        # Refresh movement
        self.movement = self.max_movement

        # Tell the scenario we're ready
        self.scenario.ship_ready(self.id)

    # ------------------------------------------------------------------------------------------------------------------
    # Methods
    # ------------------------------------------------------------------------------------------------------------------

    def queue_move(from_coord, to_coord):
        """
        Queue up a move from `from_coord` to `to_coord`. Each coord is assumed to be a dict with keys `position` and
        `rotation`. Each of these is assumed to be a tuple of the form `(x, y, z)`. So an example coord would be:

        ```python
        coord = {
            position: (1, 5, -3)
            rotation: (1, 0, -1)
        }
        ```

        While position can be any valid coordinate inside the map boundaries, rotation must be a vector, with each of
        it's components between `-1` and `1`.

        If the movement cost of the total move is more than the movement the ship has left, a `ValueError` is thrown.
        """

        move_cost = self.map.calc_distance(from_coord['position'], to_coord['position']) * self.move_cost
        orientation_cost = self.map.calc_rotation(from_coord['rotation'], to_coord['rotation']) * self.rot_cost

        # Calculate the total cost of this move
        cost = move_cost + orientation_cost

        if (self.movement < cost):
            raise ValueError("Invalid move; move cost: '{}', remaining movement: '{}'".format(cost, self.movement))

        self.__pending_movement.append({
            from_coord: from_coord,
            to_coord: to_coord,
            cost: cost
        })

    def dequeue_last_move(self):
        """ Removes the last queued move, if any. """
        self.__pending_movement.pop()

    def apply_moves(self):
        """ Apply the queued movements. """

        # Apply all the movement costs
        for move in self.__pending_movement:
            self.movement -= move.cost

        # Grab the final move, since intermediate positions/rotations don't matter
        const last_move = self.__pending_movement[-1]

        # Update position
        self.position = last_move['to_coord']['position']

        # Update rotation
        self.rotation = last_move['to_coord']['rotation']

        # Clear all pending movements
        self.__pending_movement.clear()

# ----------------------------------------------------------------------------------------------------------------------

__all__ = ['BaseShip']

# ----------------------------------------------------------------------------------------------------------------------
