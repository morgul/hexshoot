# ----------------------------------------------------------------------------------------------------------------------
# A Basic Testing Scenario
# ----------------------------------------------------------------------------------------------------------------------

from server.game.scenarios.base import BaseScenario

# ----------------------------------------------------------------------------------------------------------------------

class TestScenario(BaseScenario):
    def __init__(self):
        # We hard-code the definition file, for now
        definition = {
            'name': 'Unwinnable Testing Scenario',
            'min_players': 1,
            'max_players': 1,
            'map': {
                # TODO: Define the map
            }
        }

        # Pass to the base constructor
        super().__init__(definition)

    @property
    def winner(self):
        # There is never a winner
        return None

    def victory_conditions_met(self):
        # This scenario can never end
        return False

# ----------------------------------------------------------------------------------------------------------------------
