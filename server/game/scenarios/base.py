# ----------------------------------------------------------------------------------------------------------------------
# Scenario Base Class
# ----------------------------------------------------------------------------------------------------------------------

import uuid
from abc import ABC, abstractmethod, abstractproperty

from transitions import Machine
from pyee import BaseEventEmitter

from server.game.map import HexMap

# ----------------------------------------------------------------------------------------------------------------------

class BaseScenario(BaseEventEmitter, ABC):
    """
    The scenario class keeps track of what state it's in, and informs anything that cares about changes via events.

    Scenarios always start in the `waiting_for_players` phase, and only move out of it once either enough players have
    joined, or have clicked `ready`. From there, we stay in a tight turn look until a victory condition is hit, then it
    transitions to `complete`, emitting a `scenario_complete` event with `victory` either true or false.
    """
    __states = [
        'waiting_for_players',
        'begin_phase',
        'tactical_phase',
        'combat_phase',
        'end_phase',
        'complete'
    ]
    __transitions = [
        {
            'trigger': 'start_scenario',
            'source': 'waiting_for_players',
            'dest': 'begin_phase',
            'conditions': ['has_min_players', 'all_players_ready'],
            'after': '_clean_state'
        },
        {
            'trigger': 'ship_begin_complete',
            'source': 'begin_phase',
            'dest': 'tactical_phase',
            'conditions': ['all_ships_ready'],
            'after': '_clean_state'
        },
        {
            'trigger': 'tactical_complete',
            'source': 'tactical_phase',
            'dest': 'combat_phase',
            'conditions': ['all_players_ready'],
            'after': '_clean_state'
        },
        {
            'trigger': 'combat_complete',
            'source': 'combat_phase',
            'dest': 'end_phase',
            'conditions': ['all_players_ready'],
            'after': '_clean_state'
        },
        {
            'trigger': 'ship_end_complete',
            'source': 'end_phase',
            'dest': 'begin_phase',
            'conditions': ['all_ships_ready'],
            'unless': ['victory_conditions_met'],
            'after': '_clean_state'
        },
        {
            'trigger': 'scenario_complete',
            'source': 'end_phase',
            'dest': 'complete',
            'conditions': ['victory_conditions_met', 'all_ships_ready'],
            'after': '_clean_state'
        }
    ]

    def __init__(self, definition={}):
        super().__init__()

        self.__uuid = uuid.uuid4()
        self.__players = set()
        self.__ready_players = set()
        self.__ships = set()
        self.__ready_ships = set()
        self.__definition = definition

        self.__map = HexMap(definition['map'])
        self.__machine = Machine(
            model=self,
            states=self.__states,
            transitions=self.__transitions,
            initial='waiting_for_players'
        )

    # ------------------------------------------------------------------------------------------------------------------
    # Properties
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def id(self):
        return self.__uuid

    @property
    def name(self):
        return self.__definition.get('name', 'Unknown Scenario')

    @property
    def players(self):
        return self.__players

    @property
    def ships(self):
        return self.__ships

    @property
    def map(self):
        return self.__map

    @property
    def min_players(self):
        return self.__definition.get('min_players', 1)

    @property
    def max_players(self):
        return self.__definition.get('max_players', 1)

    @abstractproperty
    @property
    def winner(self):
        pass

    # ------------------------------------------------------------------------------------------------------------------
    # Transitions
    # ------------------------------------------------------------------------------------------------------------------

    def on_exit_waiting_for_players(self):
        self._phase_changed()

    def on_exit_begin_phase(self):
        self._phase_changed()

    def on_exit_tactical_phase(self):
        self._phase_changed()

    def on_exit_combat_phase(self):
        self._phase_changed()

    def on_enter_end_phase(self):
        if (self.victory_conditions_met()):
            self.scenario_complete()

    def on_exit_end_phase(self):
        self._phase_changed()

    def on_enter_complete(self):
        self.emit('scenario_complete', self.winner)

    # ------------------------------------------------------------------------------------------------------------------
    # Conditions
    # ------------------------------------------------------------------------------------------------------------------

    def all_players_ready(self):
        return len(self.__ready_players) == len(self.__players)

    def has_min_players(self):
        return len(self.__players) >= self.min_players

    def has_max_players(self):
        return len(self.__players) == self.max_players

    def all_ships_ready(self):
        return len(self.__ready_ships) == len(self.__ships)

    @abstractmethod
    def victory_conditions_met(self):
        pass

    # ------------------------------------------------------------------------------------------------------------------
    # Internal
    # ------------------------------------------------------------------------------------------------------------------

    def _phase_changed(self):
        """ Handle phase changing """
        self.emit('phase_changed', self.state)

    def _clean_state(self):
        """ Clean up scenario's state """
        self.__ready_players.clear()
        self.__ready_ships.clear()

    # ------------------------------------------------------------------------------------------------------------------
    # Methods
    # ------------------------------------------------------------------------------------------------------------------

    def join(self, player):
        """ A player has joined the scenario. """
        self.__players.add(player)

    def leave(self, player):
        """ A player has left the scenario """
        self.__ready_players.remove(player.id)
        self.__players.remove(player)

        # A player who was not ready leaving might push us over the threshold, so we have to check
        if (self.all_players_ready() and self.has_min_players()):
            self.start_scenario()

    def player_ready(self, player_id):
        """ A player has marked themselves ready """
        if (self.state not in ['waiting_for_players', 'tactical_phase', 'combat_phase']):
            raise AssertionError("Ready is not available in state '{}'".format(self.state))

        # Mark the player as ready
        self.__ready_players.add(player_id)

        # Check to see if we're ready to transition
        if (self.has_max_players):
            self.start_scenario()
        elif (self.all_players_ready() and self.has_min_players()):
            self.start_scenario()

    def ship_ready(self, ship_id):
        """ A ship has completed it's phase change tasks """
        if (self.state not in ['begin_phase', 'end_phase']):
            raise AssertionError("Ready is not available in state '{}'".format(self.state))

        # Mark the player as ready
        self.__ready_ships.add(ship_id)

        # Check to see if we're ready to transition
        if (self.all_ships_ready()):
            if (self.state == 'begin_phase'):
                self.ship_begin_complete()
            elif (self.state == 'end_phase'):
                self.ship_end_complete()

    def to_dict(self):
        return {
            'id': str(self.id),
            'name': self.name,
            'players': [str(player.id) for player in self.players],
            'ships': [str(ship.id) for ship in self.ships],
            'min_players': self.min_players,
            'max_players': self.max_players
        }

# ----------------------------------------------------------------------------------------------------------------------

__all__ = ['BaseScenario']

# ----------------------------------------------------------------------------------------------------------------------
