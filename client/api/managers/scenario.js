//----------------------------------------------------------------------------------------------------------------------
// ScenarioManager
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject } from 'rxjs';

// Resource Access
import scenarioRA from '../resource-access/scenario';

//----------------------------------------------------------------------------------------------------------------------

class ScenarioManager
{
    constructor()
    {
        // Subjects
        this._currentScenarioSubject = new BehaviorSubject();
        this._scenarioTypesSubject = new BehaviorSubject([]);
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Observables
    //------------------------------------------------------------------------------------------------------------------

    get currentScenario$() { return this._currentScenarioSubject.asObservable(); }
    get scenarioTypes$() { return this._scenarioTypesSubject.asObservable(); }

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    get currentScenario() { return this._currentScenarioSubject.getValue(); }
    get scenarioTypes() { return this._scenarioTypesSubject.getValue(); }

    //------------------------------------------------------------------------------------------------------------------
    // Properties
    //------------------------------------------------------------------------------------------------------------------

    async listScenarioTypes()
    {
        return scenarioRA.listScenarioTypes()
            .then((scenarioTypes) =>
            {
                this._scenarioTypesSubject.next(scenarioTypes);
            });
    } // end listScenarioTypes

    async createScenario(scenarioType)
    {
        return scenarioRA.createScenario(scenarioType)
            .then((scenario) =>
            {
                this._currentScenarioSubject.next(scenario);
                return scenario;
            });
    } // end createScenario
} // end ScenarioManager

//----------------------------------------------------------------------------------------------------------------------

export default new ScenarioManager();

//----------------------------------------------------------------------------------------------------------------------
