//----------------------------------------------------------------------------------------------------------------------
// SocketUtil
//----------------------------------------------------------------------------------------------------------------------

import EventEmitter from 'events';
import io from 'socket.io-client';

//----------------------------------------------------------------------------------------------------------------------

class SocketUtil extends EventEmitter
{
    constructor()
    {
        super();
        this.$socket = io();

        this.outstandingRequests = new Map();

        // Bind events
        this.$socket.on('connect', this._onConnected.bind(this));
        this.$socket.on('reconnect', this._onConnected.bind(this));
        this.$socket.on('error', this._onError.bind(this));
        this.$socket.on('disconnect', this._onDisconnected.bind(this));
        this.$socket.on('reconnecting', this._onDisconnected.bind(this));
        this.$socket.on('reconnect_failed', this._onReconnectFailed.bind(this));
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Event Handlers
    //------------------------------------------------------------------------------------------------------------------

    _onConnected()
    {
        console.info('Socket.io connected.');
        this.emit('connected');
    } // end _onConnected

    _onError(error)
    {
        console.error('Socket.io error:', error);
    } // end _onError

    _onDisconnected(reason)
    {
        if(reason == 'io server disconnect')
        {
            console.warn('Server rejected connection. (Sending the user back to the main page.)');
            window.location = '/';
        } // end if

        this.emit('disconnected');
    } // end _onDisconnected

    _onReconnectFailed()
    {
        console.warn('Failed to reconnect. (Sending the user back to the main page.)');
        window.location = '/';
    } // end _onReconnectFailed

    //------------------------------------------------------------------------------------------------------------------
    // Public Functions
    //------------------------------------------------------------------------------------------------------------------

    /**
     * Requests are always expected to respond with a payload of some kind; at the very least, they wait until the
     * server responds.
     *
     * @param {string} operation - the name of the operation to perform
     * @param {object} payload - any additional arguments the request requires
     * @param {string} [room] - a string name for the room to send the request to
     *
     * @returns {Promise} - Returns a promise that resolves once the request is complete
     */
    request(operation, payload = {}, room)
    {
        const key = `${ operation }:${ JSON.stringify(payload) }`;
        let outstandingRequest = this.outstandingRequests.get(key);
        if(!outstandingRequest)
        {
            outstandingRequest = new Promise((resolve, reject) =>
            {
                const socket = room ? this.$socket.to(room) : this.$socket;
                socket.emit(operation, payload, ({ error, payload }) =>
                {
                    // Delete our outstanding request
                    this.outstandingRequests.delete(key);

                    if(error)
                    {
                        // Unpack service errors
                        if(error.error)
                        {
                            error.error.name = 'ServiceError';
                            error.error.code = 'ERR_SERVICE';
                            error = error.error;
                        } // end if

                        // Deserialize the error object
                        const err = new Error(error.message);
                        Object.assign(err, error);

                        console.error(`Service Error <${ operation }>: ${ error.message }`, error);
                        reject(err);
                    }
                    else
                    {
                        resolve(payload);
                    } // end if
                });
            });

            this.outstandingRequests.set(key, outstandingRequest);
        } // end if

        return outstandingRequest;
    } // end request

    /**
     * Commands are 'fire and forget' messages with no responses. They are for triggering things that may trigger one
     * or more events at a later time, without the need for an immediate response.
     *
     * @param {string} operation - the name of the operation to perform
     * @param {object} payload - any additional arguments the request requires
     * @param {string} [room] - a string name for the room to send the request to
     *
     * @returns {void}
     */
    command(operation, payload = {}, room)
    {
        const socket = room ? this.$socket.to(room) : this.$socket;
        socket.emit(operation, payload);
    } // end command

    /**
     * Listen for a specific event from the server.
     *
     * @param {string} operation - the name of the operation to perform
     * @param {function} handler - a handler function to call when the event is triggered
     * @param {string} [room] - a string name for the room to send the request to
     *
     * @returns {void}
     */
    event(operation, handler, room)
    {
        const socket = room ? this.$socket.to(room) : this.$socket;
        socket.on(operation, handler);
    } // end event
} // end SocketUtil

//----------------------------------------------------------------------------------------------------------------------

export default new SocketUtil();

//----------------------------------------------------------------------------------------------------------------------
