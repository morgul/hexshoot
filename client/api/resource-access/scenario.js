//----------------------------------------------------------------------------------------------------------------------
// ScenarioResourceAccess
//----------------------------------------------------------------------------------------------------------------------

// Utils
import socketUtil from '../util/socket';

//----------------------------------------------------------------------------------------------------------------------

class ScenarioResourceAccess
{
    async listScenarioTypes()
    {
        return socketUtil.request('list_scenario_types');
    } // end listScenarioTypes

    async createScenario(scenarioType)
    {
        return socketUtil.request('create_scenario', { type: scenarioType });
    } // end createScenario
} // end ScenarioResourceAccess

//----------------------------------------------------------------------------------------------------------------------

export default new ScenarioResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
