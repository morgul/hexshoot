//----------------------------------------------------------------------------------------------------------------------
// PlayerResourceAccess
//----------------------------------------------------------------------------------------------------------------------

import { BehaviorSubject } from 'rxjs';

// Utils
import socketUtil from '../util/socket';

// Model
import Player from '../models/player';

//----------------------------------------------------------------------------------------------------------------------

class PlayerResourceAccess
{
    constructor()
    {
        this._currentPlayer = undefined;
        this._players = {};

        // Subjects
        this._currentPlayerSubject = new BehaviorSubject();

        // Event Handlers
        socketUtil.event('current_player', this._onCurrentPlayer.bind(this));
    } // end constructor

    //------------------------------------------------------------------------------------------------------------------
    // Event Handlers
    //------------------------------------------------------------------------------------------------------------------

    _onCurrentPlayer(playerDef)
    {
        const player = this._buildModel(playerDef);
        this._currentPlayerSubject.next(player);
    } // end _onCurrentPlayer

    //------------------------------------------------------------------------------------------------------------------
    // Internal
    //------------------------------------------------------------------------------------------------------------------

    _buildModel(def)
    {
        let player = this._players[def.id];
        if(player)
        {
            player.update(def);
        }
        else
        {
            player = new Player(def);
            this._players[def.id] = player;
        } // end if

        return player;
    } // end _buildModel

    //------------------------------------------------------------------------------------------------------------------
    // Methods
    //------------------------------------------------------------------------------------------------------------------

    async setCurrentName(name)
    {
        const player = await socketUtil.request('set_name', { name });
        return this._buildModel(player);
    } // end setCurrentName
} // end PlayerResourceAccess

//----------------------------------------------------------------------------------------------------------------------

export default new PlayerResourceAccess();

//----------------------------------------------------------------------------------------------------------------------
